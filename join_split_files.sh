#!/bin/bash

cat system/system/priv-app/HMDCamera/HMDCamera.apk.* 2>/dev/null >> system/system/priv-app/HMDCamera/HMDCamera.apk
rm -f system/system/priv-app/HMDCamera/HMDCamera.apk.* 2>/dev/null
cat system/system/apex/com.android.art.release.apex.* 2>/dev/null >> system/system/apex/com.android.art.release.apex
rm -f system/system/apex/com.android.art.release.apex.* 2>/dev/null
cat system/system/apex/com.android.vndk.current.apex.* 2>/dev/null >> system/system/apex/com.android.vndk.current.apex
rm -f system/system/apex/com.android.vndk.current.apex.* 2>/dev/null
cat system/system/product/priv-app/Velvet/Velvet.apk.* 2>/dev/null >> system/system/product/priv-app/Velvet/Velvet.apk
rm -f system/system/product/priv-app/Velvet/Velvet.apk.* 2>/dev/null
cat system/system/product/priv-app/GmsCore/GmsCore.apk.* 2>/dev/null >> system/system/product/priv-app/GmsCore/GmsCore.apk
rm -f system/system/product/priv-app/GmsCore/GmsCore.apk.* 2>/dev/null
cat system/system/product/app/TrichromeLibrary/TrichromeLibrary.apk.* 2>/dev/null >> system/system/product/app/TrichromeLibrary/TrichromeLibrary.apk
rm -f system/system/product/app/TrichromeLibrary/TrichromeLibrary.apk.* 2>/dev/null
cat system/system/product/app/LatinImeGoogle/LatinImeGoogle.apk.* 2>/dev/null >> system/system/product/app/LatinImeGoogle/LatinImeGoogle.apk
rm -f system/system/product/app/LatinImeGoogle/LatinImeGoogle.apk.* 2>/dev/null
cat system/system/product/app/Gmail2/Gmail2.apk.* 2>/dev/null >> system/system/product/app/Gmail2/Gmail2.apk
rm -f system/system/product/app/Gmail2/Gmail2.apk.* 2>/dev/null
cat system/system/product/app/Messages/Messages.apk.* 2>/dev/null >> system/system/product/app/Messages/Messages.apk
rm -f system/system/product/app/Messages/Messages.apk.* 2>/dev/null
cat system/system/product/app/WebViewGoogle/WebViewGoogle.apk.* 2>/dev/null >> system/system/product/app/WebViewGoogle/WebViewGoogle.apk
rm -f system/system/product/app/WebViewGoogle/WebViewGoogle.apk.* 2>/dev/null
cat system/system/product/app/Maps/Maps.apk.* 2>/dev/null >> system/system/product/app/Maps/Maps.apk
rm -f system/system/product/app/Maps/Maps.apk.* 2>/dev/null
cat system/system/product/app/Photos/Photos.apk.* 2>/dev/null >> system/system/product/app/Photos/Photos.apk
rm -f system/system/product/app/Photos/Photos.apk.* 2>/dev/null
cat system/system/product/app/YouTube/YouTube.apk.* 2>/dev/null >> system/system/product/app/YouTube/YouTube.apk
rm -f system/system/product/app/YouTube/YouTube.apk.* 2>/dev/null
cat system/system/system_ext/priv-app/Settings/Settings.apk.* 2>/dev/null >> system/system/system_ext/priv-app/Settings/Settings.apk
rm -f system/system/system_ext/priv-app/Settings/Settings.apk.* 2>/dev/null
cat boot.img.* 2>/dev/null >> boot.img
rm -f boot.img.* 2>/dev/null
