#
# Copyright (C) 2023 The Android Open Source Project
# Copyright (C) 2023 SebaUbuntu's TWRP device tree generator
#
# SPDX-License-Identifier: Apache-2.0
#

PRODUCT_MAKEFILES := \
    $(LOCAL_DIR)/omni_DDV_sprout.mk

COMMON_LUNCH_CHOICES := \
    omni_DDV_sprout-user \
    omni_DDV_sprout-userdebug \
    omni_DDV_sprout-eng
